Instrucciones de instalación.

Para la ejecución de pruebas unitarias (Y cualquier otra necesidad de Zend Framework) será necesario instalar Composer: https://getcomposer.org/ (Zend framwork 3 trabaja con él).

Breve análisis y requerimiento del proyecto: https://docs.google.com/document/d/1rEi3a_rDeKqV--q5GOsrDA6ERP9kAIc6BDEpWix-jbw/edit

Ya clonado mi proyecto de Zend Framework (git clone https://EmmanuelSalazarT@bitbucket.org/EmmanuelSalazarT/cash-machine-emmanuel.git), se ha de instalar en un servidor http, el host (o virtual host) debe de apuntar a la carpeta "/public" del proyecto.

(En otras situaciones, habría que utilizar Composer para instalar las dependencias de Zend Framework; pero en este caso subí el proyecto completo para que no sea necesario instalar las dependencias).

Para exportar la base de datos se ha de ejecutar el archivo "cash_machine_database.sql" que se encuentra en la raíz del proyecto. El manejado que utilicé fue Maria DB (De hecho, no me quise complicar más la vida y utilicé el XAMPP para instalar mi ambiente de desarrollo).

La base de datos se llama "cash_machine"; para conectarse a ésta, se requiere un usuario con el nombre "cash_machine_sys" y de contraseña "Habrá que cambiarla consecutivamente." (Igualmente se puede cambiar esta configuración en el archivo "/config/autoload/global.php"; en este archivo se puede editar la cadena de conexión con la base de datos así como las credenciales (Las buenas prácticas con Zend Framwork dictan que estas últimas deben de estar dentro del archivo local.php y que éste se encuentre en el .gitignore para los desarrollo den local, pero supongo que aquí no es necesario aplicarlas).

El diagrama de la base de datos se encuentra en el archivo "cash_machine_db_diagram.mwb" el cual se abre con el programa "MySQL WorkBench" (Aún así, creé una captura de este diagrama en la imagen "captura_diagrama_bd.PNG").
Como podrán ver, el sistema cuenta con solo 4 tablas: catálogo de personas, catálogo de usuarios que están vinculadas a una persona, catálogo de cuentas, que por convención definen el comportamiento de una cuenta de débito, y una extensión de este último catálogo (credit_account) que define el modelo de una cuenta de crédito extendido de una de débito.

La base de datos ya cuenta con algunos registros de pruebas, entre ellos los registros que yo he creado específicamente para las pruebas unitarias: Las cuentas con ids 13, 15 y 16 que representan una cuenta de débito y una de crédito respectivamente, las cuales están relacionadas al usuario con id 2 (Y a su vez a la persona con id 2).

Pruebas unitarias:

Utilicé la herramienta de pruebas unitarias que ofrece Zend Framework a través de PHPUnit; éstas están codificadas en el archivo "/module/CashMachine/test/Controller/CashMachineControllerText.php" y para ejecutar dichas pruebas es necesario ejecutar el comando (con la consola posicionada en la carpeta raíz del proyecto):
"vendor/bin/phpunit"

Deberá verse una salida de este tipo:

PHPUnit 7.5.14 by Sebastian Bergmann and contributors.

.Cash-machine Method Get Without Params
.Withdraw Debit Account Without Money
.Withdraw Debit Account With Enought Money
.Depost Debit Account
.Withdraw Credit Account Without Money, adding the commision
.Withdraw Credit Account With Enought Money
.Depost Credit Account with more money over the limit
.Depost Credit Account with more money under the limit
.                                                           9 / 9 (100%)Deposit Debit Account Non Existent


Time: 574 ms, Memory: 12.00 MB

OK (9 tests, 13 assertions)

Todos los casos de prueba se ejecutan sobre la url "/cash-machine".
Los casos de pruebas que se hacen son:

-Consumir directamente la ruta "/cash-machine" sin parámetros, cuya función no está definida en la api rest y por lo tal debe de arrojar un error 405.
-Intentar retirar dinero (Petición POST) de una cuenta sin fondos (id 13) por lo que arrojará un error 500.
-Intentar retirar $1,000 de una cuenta con suficientes fondos (id 15) por lo cual arrojará una respuesta 200.
-Intentar depositar (Petición PUT) $1,000 a la cuenta anterior, por lo cual arrojará una respuesta 200.
-Intentar retirar $5,000 de una cuenta (id 16) de crédito con $5,000 de dinero disponible, debido a la comisión del 10% ésto no será posible y arrojará un error 500.
-Intentar retirar $1,000 de la misma cuenta de crédito anterior, lo cual sí podrá y, con la comisión, retirará $1,100.
-Intentar depositar $11,000 en la cuenta de crédito anterior (id 16) siendo que ésta tiene un límite de $10,000 (Definido en la tabla "credit_account") Por lo que arrojará un error 500.
-Intentar depositar $1,100 a la misma cuenta de crédito lo cual no rebasará el límite y (de hecho) repondrá lo deducido de la anterior operación de retiro ($1000 + 100 de comisión).
-Intentará hacer un depósito en una cuenta inexistente (-1) por lo que arrojará un error 500.


Los servicios regresan originalmente un Json y los controladores extienden de "\Zend\Mvc\Controller\AbstractRestfulController" (Que es la implementación de Zend Framework para las ApiRestful), y Para consumir manualmente los servicios:

Para obtener los datos de un usuario (Datos de usuario y cuentas) se ha de consumir el servicio

GET: /user?id?{user_id}

Ésto esta programado en el controller "/module/user/src/controller/UserController.php" en el action "get($id)".

Los servicios de la "cash machine" se encuentran en el controller "/module/CashMachine/src/controller/CashMachineController.php" y se pueden consumir de la url
"/cash-machine".

Para depositar dinero a una cuenta se hace la siguiente petición:

PUT: /cash-machine?id={account_id}
body: {
	"amount": {amount_to_deposit}
} //El cuerpo en un objeto Json.

Para retirar dinero de una cuenta se hace la siguiente petición:

POST: /cash-machine
body {
	"id": {account_id},
	"amount": {amount_to_withdraw}
}

Para crear una cuenta (Débito y crédito) se consume el servicio /account

POST: /account
{
	"number": {numero_de_cuenta (El número es único y de 8 caracteres de longitud)},
	"user_id": {id_de_usuario},
	"type": {"CREDIT"/"DEBIT"},
	"amount_limit": {float, límite de la cuenta, en caso de ser de crédito}
}

GET: /account (Simplemente arroja la lista de cuentas creadas).

GET: /account?id={id_account} //Busca una cuenta por ID.

Para crear un usuario se consume el servicio /user:

POST: /user
{
	"name": {string, min 4 chars lenght},
	"first_surname": {string, min 4 chars lenght},
	"second_surname": {string, no required},
	"curp": {string curp, 18 chars lenght},
	"rfc": {string rfc, 13 chars lenght, not required},
	"number": {int, user number},
	"nip": {int, user nip, 4 chars lenght}
}

GET: /user?id={id_user} //Busca un usuario por ID

En cuestiones de errores, no tuve tiempo para ver cómo deshabilitar el renderizado automático de la vista de error que maneja Zend Framwork para enviar un JSON son la información del error.

La estructura de las tablas de usuario (y persona) es la siguiente:

Tabla person: //La tabla donde se guardan los datos de persona de los usuarios (Puesta a parte porque se considera que también se manejarán datos de personas no usuarios).
name => nombre, string, obligatorio,
first_surname => primer apellido, string, obligatorio.
second_surname => segundo apellido, string, no obligatorio.
curp => curp, string, obligatorio,
rfc => rfc, string, no obligatorio.

Tabla user: La tabla de usuarios dueños de alguna cuenta o más.
number: número de cuenta, numérico, obligatorio.
nip: nip de usuario, numérico, obligatorio.
person_id: llave foránea a la persona que está relacionado el usuario, int, obligatorio.


La estructura de las tablas de cuentas es la siguiente:

Tabla acount: //Tabla donde se guardan la información "básica" de las cuentas, por convención es la que guarda toda la información de una cuenta de débito. Aunque no llegué a implementar un servicio para guardarlo, creé un builder para este objeto que se encuentra en /module/Account/src/Model/AccountBuilder.php
number => número de cuenta, entero (8 dígitos), obligatorio,
amount_available => cantidad disponible de la cuenta, float, obligatorio, por defecto 0,
user_id => id del usuario dueño de la cuenta, llave foránea, entero, obligatorio.
type => typo de cuenta, enum(CREDIT,DEBIT), obligatorio.

Tabla credit_account: //Tabla que extiende de account y especifica los datos para las tarjetas que son del tipo (type) DEBITO.
account_id => id de la cuenta a la que extiende, llave foránea, entero, obligatorio.
amount_limit => Límite de crédito, float, obligatorio.
commission_withdraw => porcentaje de comisión de retiro, float, entre 0 y 1, obligatorio.