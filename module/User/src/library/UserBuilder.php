<?php
namespace User;

class UserBuilder
{
    private $user;
            
    public function __construct(string $name,string $firstSurname, string $secondSurname,int $number)
    {
        $this->user = new User();
        $this->user->setName($name);
        $this->user->setFirstSurname($firstSurname);
        $this->user->setSecondSurname($secondSurname);
        $this->user->setNumber($number);
    }
    
    public function setCurp(string $curp)
    {
        $this->user->setCurp($curp);
        return $this;
    }
    
    public function setRfc(string $rfc)
    {
        $this->user->setRfc($rfc);
        return $this;
    }
    
    public function createUser()
    {
        $this->user->save();
        
        return $this->user;
    }
    
}