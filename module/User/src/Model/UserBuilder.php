<?php
namespace User\Model;

class UserBuilder
{
    protected $user;
    
    public function __construct(int $userNumber) 
    {
        $this->user = new User();
        $this->user->setNumber($userNumber);
    }
    
    public function setNip(int $nip)
    {
        if(strlen((string)$nip)!=4)
        {
            throw new Exception('The nip must be 4 chars lenght');
        }
        $this->user->setNip($nip);
        return $this;
    }
    
    public function setName(string $name)
    {
        if(strlen($name)>=4)
        {
            $this->user->setName($name);
        }
        else
        {
            throw new \Exception("The name must be 4 chars lenght or more.");
        }
    }
    
    public function setFirstSurname(string $firstSurname)
    {
        if(strlen($firstSurname)>=4)
        {
            $this->user->setFirstSurname($firstSurname);
        }
        else
        {
            throw new \Exception("The first surname must be 4 chars lenght or more.");
        }
    }
    
    public function setSecondSurname(string $secondSurname)
    {
        $this->user->setSecondSurname($secondSurname);
    }
    
    public function setCurp(string $curp)
    {
        if(strlen($curp)!=18)
        {
            throw new \Exception("The curp must be 18 chars lenght.");
        }
        $this->user->setCurp($curp);
    }
    
    public function setRfc(string $rfc)
    {
        if(strlen($rfc)!=13)
        {
            throw new \Exception("The rfc must be 13 chars lenght.");
        }
        $this->user->setRfc($rfc);
    }
    
    public function buildUser()
    {
        if($this->user->getNumber()==null || !$this->user->getNumber())
        {
            throw new \Exception("Missing the user's number.");
        }
        if($this->user->getNip()==null || !$this->user->getNip())
        {
            throw new \Exception("Missing the user's nip.");
        }
        if($this->user->getName()==null || !$this->user->getName())
        {
            throw new \Exception("Missing the user's name.");
        }
        if($this->user->getFirstSurname()==null || !$this->user->getFirstSurname())
        {
            throw new \Exception("Missing the user's first surname.");
        }
        if($this->user->getCurp()==null || !$this->user->getCurp())
        {
            throw new \Exception("Missing the user's curp.");
        }
        
        $this->user->save();
        return $this->user;
    }
    
}