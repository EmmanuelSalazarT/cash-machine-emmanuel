<?php
namespace User\Model;

class UserConector
{
    private static $accountTable;
    private static $userTable;
    private static $personTable;
    
    public static function setAccountTableConector(\Account\Model\DebitAccountTable $debitAccountTable)
    {
        if(!isset(self::$accountTable))
        {
            self::$accountTable = $debitAccountTable;
        }
    }
    
    public static function getAccountTableConector()
    {
        return self::$accountTable;
    }
    
    public static function setUserTableConector(\User\Model\UserTable $userTable)
    {
        if(!isset(self::$userTable))
        {
            self::$userTable = $userTable;
        }
    }
    
    public static function getUserTableConector()
    {
        return self::$userTable;
    }
    
    public static function setPersonTableConector(\Person\Model\PersonTable $personTable)
    {
        if(!isset(self::$personTable))
        {
            self::$personTable = $personTable;
        }
    }
    
    public static function getPersonTableConector()
    {
        return self::$personTable;
    }
}