<?php
namespace User\Model;

use Person\Model\Person;

class User extends Person{
    
    protected $person_id;
    protected $number;
    protected $nip;
    protected $userId;
    protected $accounts = array();
    
    public function exchangeArray(array $data)
    {
        parent::exchangeArray($data);
        $this->person_id     = !empty($data['person_id']) ? $data['person_id'] : null;
        $this->number = !empty($data['number']) ? $data['number'] : null;
        $this->nip  = !empty($data['nip']) ? $data['nip'] : null;
        $this->userId = !empty($data['user_id'])? $data["user_id"] : null;
    }
    
    public function getUserId(){
        return $this->userId;
    }
    
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
    }

    function getPerson_id() {
        return $this->person_id;
    }

    function getNumber() {
        return $this->number;
    }

    function getNip() {
        return $this->nip;
    }

    function setPerson_id(Int $person_id) {
        $this->person_id = $person_id;
    }

    function setNumber(Int $number) {
        $this->number = $number;
    }

    function setNip(Int $nip) 
    {
        if(strlen((string)$nip)!=4)
        {
            throw new \Exception('The nip must be 4 chars lenght');
        }
        $this->nip = $nip;
    }
    
    public function fetchAccounts()
    {
        $this->accounts = \CashMachine\Model\CashMachineConector::getAccountTableConector()->findByUserId($this->getId());
    }
    
    protected function validateAttributesForSave()
    {
        if(!isset($this->number))
        {
            throw new \Exception('The number can\'t be empty');
        }
        if(!isset($this->nip))
        {
            throw new \Exception('The nip can\'t be empty');
        }
        return parent::validateAttributesForSave();
    }
    
    function save() {
        $this->validateAttributesForSave();
        parent::save();
        UserConector::getUserTableConector()->saveUser($this);
        
        return $this;
    }

}