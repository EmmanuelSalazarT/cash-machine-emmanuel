<?php
namespace User\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

class UserTable
{
    private $tableGateway;
    
    public function __construct(TableGatewayInterface $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function getUser($id)
    {
        $id  = (int) $id;
        $sql = new \Zend\Db\Sql\Sql( $this->tableGateway->adapter ) ;
        
        $where = new \Zend\Db\Sql\Where();
        $where -> equalTo( 'user.id', $id ) ;
        $select = $sql->select() ;
        $select -> from ( $this->tableGateway->getTable() )
            ->columns(array("id as user_id","number","nip","person_id"))
            -> join ( 'person' , 'user.person_id = person.id',array("id","name","first_surname","second_surname","curp","rfc"),'left')
            -> where( $where ) ;
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        $user = new User();
        $user->exchangeArray($row);
        $user->fetchAccounts();
        return $user;
    }
    
    public function saveUser(User $user)
    {
        $data = array(
            'id' => ($user->getUserId()!= null )?$user->getUserId():null,
            'number' => $user->getNumber(),
            'nip'  => $user->getNip(),
            'person_id' => $user->getId(),
        );
        if ($user->getUserId()!=null && $user->getUserId()>0 ) {
             $this->tableGateway->update($data, array('id' =>$user->getUserId()));
             return $user;
        } else {
             $this->tableGateway->insert($data);
             $user->setUserId($this->tableGateway->lastInsertValue);
             return $user;
        }
    }
}