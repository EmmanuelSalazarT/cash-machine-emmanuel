<?php
namespace User;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    
     public function getServiceConfig()
    {
        return [
            'factories' => [
                Account\Model\DebitAccountTable::class => function($container) {
                    $tableGateway = $container->get(Account\Model\DebitAccountTableGateway::class);
                    return new \Account\Model\DebitAccountTable($tableGateway);
                },
                Account\Model\DebitAccountTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Account\Model\DebitAccount());
                    return new TableGateway('account', $dbAdapter, null, $resultSetPrototype);
                },
                Model\UserTable::class => function($container) {
                    $tableGateway = $container->get(Model\UserTableGateway::class);
                    return new Model\UserTable($tableGateway);
                },
                Model\UserTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
                        
                Person\Model\PersonTable::class => function($container) {
                    $tableGateway = $container->get(Person\Model\PersonTableGateway::class);
                    return new \Person\Model\PersonTable($tableGateway);
                },
                Person\Model\PersonTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Person\Model\Person());
                    return new TableGateway('person', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
    
    public function getControllerConfig()
    {
        return [
            'factories' => [
            Controller\UserController::class => function($container) {
                    return new Controller\UserController(
                        $container->get(Model\UserTable::class),
                        $container->get(Account\Model\DebitAccountTable::class),
                        $container->get(Person\Model\PersonTable::class)
                    );
                },
            ],
        ];
    }
}