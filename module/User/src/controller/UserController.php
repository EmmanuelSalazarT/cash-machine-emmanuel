<?php
namespace User\Controller;

use Zend\View\Model\JsonModel;
use User\Model\UserTable;
use User\Model\UserConector;


class UserController extends \Zend\Mvc\Controller\AbstractRestfulController {
    
    public function __construct(UserTable $userTable, \Account\Model\DebitAccountTable $debitAccountTable, \Person\Model\PersonTable $personTable)
    {
        UserConector::setAccountTableConector($debitAccountTable);
        UserConector::setUserTableConector($userTable);
        \CashMachine\Model\CashMachineConector::setAccountTableConector($debitAccountTable);
        UserConector::setPersonTableConector($personTable);
    }
    
    /*
     * Get userObject by Id
     * 
     * @params int $id; user's id to find;
     * 
     * return User object;
     */
    public function get($id)
    {
        $id = (int) $id;
            
        $user = UserConector::getUserTableConector()->getUser($id);
        
        $response = new \Application\Model\ResponseBuilder();
        $response->createSuccesResponse();
        $response->setResponse($user);
        
        return new JsonModel( $response->buildJsonResponse());
    }
    
    /*
     * 
     */
    public function create($data)
    {
        $response = new \Application\Model\ResponseBuilder();
        try
        {
            $name = (isset($data["name"]))? $data["name"] : null;
            $firstSurname = (isset($data["first_surname"]))? $data["first_surname"] : null; 
            $secondSurname = (isset($data["second_surname"]))? $data["second_surname"] : null;
            $curp = (isset($data["curp"]))? $data["curp"] : null;
            $rfc = (isset($data["rfc"]))? $data["rfc"] : null;
            $number = (isset($data["number"]))? $data["number"] : null;
            $nip = (isset($data["nip"]))? $data["nip"] : null;
            
            $builder = new \User\Model\UserBuilder($number);
            $builder->setCurp($curp);
            $builder->setFirstSurname($firstSurname);
            $builder->setName($name);
            $builder->setNip($nip);
            if($rfc!=null)
            {
                $builder->setRfc($rfc);
            }
            $builder->setSecondSurname($secondSurname);
            $user = $builder->buildUser();
            $response->createSuccesResponse();
            $response->setResponse($user);
        } 
        catch (Exception $ex) 
        {
            $response->createErrorResponse($ex->getMessage());
        }
        return new JsonModel( $response->buildJsonResponse());
    }
}
