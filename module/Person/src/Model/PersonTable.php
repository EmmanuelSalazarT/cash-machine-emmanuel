<?php
namespace Person\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

class PersonTable
{
    private $tableGateway;
    
    public function __construct(TableGatewayInterface $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function savePerson(Person $person)
    {
        $data = array(
            'id' => ($person->getId()!= null )?$person->getId():null,
            'name' => $person->getName(),
            'first_surname'  => $person->getFirstSurname(),
            'second_surname' => $person->getSecondSurname(),
            'curp' => $person->getCurp(),
            'rfc' => $person->getRfc(),
        );
        if ($person->getId()!=null && $person->getId()>0 ) {
             $this->tableGateway->update($data, array('id' => $person->getId()));
             return $person;
        } else {
             $this->tableGateway->insert($data);
             $person->setId($this->tableGateway->lastInsertValue);
        }
    }
}