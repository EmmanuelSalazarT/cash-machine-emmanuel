<?php
namespace Person\Model;

class Person implements \JsonSerializable{
    private $id;
    private $name;
    private $firstSurname;
    private $secondSurname;
    private $curp;
    private $rfc;
    
    public function exchangeArray(array $data)
    {
        $this->id     = !empty($data['id']) ? $data['id'] : null;
        $this->name = !empty($data['name']) ? $data['name'] : null;
        $this->firstSurname  = !empty($data['first_surname']) ? $data['first_surname'] : null;
        $this->secondSurname     = !empty($data['second_surname']) ? $data['second_surname'] : null;
        $this->curp     = !empty($data['curp']) ? $data['curp'] : null;
        $this->rfc     = !empty($data['rfc']) ? $data['rfc'] : null;
    }
    
    public function setId(int $id)
    {
        if($this->id == null)
        {
            $this->id = $id;
        }
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setName(String $name)
    {
        if(strlen($name)>=4)
        {
            $this->name = $name;
        }
        else
        {
            throw new \Exception("The  name must be 4 chars lenght or more.");
        }
    }
    
    public function getFirstSurname() {
        return $this->firstSurname;
    }

    public function getSecondSurname() {
        return $this->secondSurname;
    }

    public function getCurp() {
        return $this->curp;
    }

    public function getRfc() {
        return $this->rfc;
    }

    public function setFirstSurname(string $firstSurname) {
        $this->firstSurname = $firstSurname;
    }

    public function setSecondSurname(String $secondSurname) {
        $this->secondSurname = $secondSurname;
    }

    public function setCurp(String $curp) 
    {
        if(strlen($curp)!=18)
        {
            throw new \Exception("The curp must be 18 chars lenght.");
        }
        $this->curp = $curp;
    }

    public function setRfc(String $rfc) {
        $this->rfc = $rfc;
    }
    
    protected function validateAttributesForSave()
    {
        if(!isset($this->name))
        {
            throw new \Exception('The name can\'t be empty.');
        }
        if(!isset($this->firstSurname))
        {
            throw new \Exception('The first first surname can\'t be empty.');
        }
        if(!isset($this->curp))
        {
            throw new \Exception('The curp can\'t be empty.');
        }
        return true;
    }
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
    
    public function save()
    {
        $this->validateAttributesForSave();
        \User\Model\UserConector::getPersonTableConector()->savePerson($this);
        
        return $this;
    }
}
