<?php
namespace Person\Model;

class PersonBuilder {
    
    private $person;
    
    public function __construct(string $name,string $firstSurname, string $secondSurname) 
    {
        $this->person = new Person();
        $this->person->setName($name);
        $this->person->setFirstSurname($firstSurname);
        $this->person->setSecondSurname($secondSurname);
    }
    
    public function setCurp(string $curp)
    {
        $this->person->setCurp($curp);
        return $this;
    }
    
    public function setRfc(string $rfc)
    {
        $this->person->setRfc($rfc);
        return $this;
    }
    
    public function createPerson()
    {
        $this->person->save();
        
        return $this->person;
    }
}
