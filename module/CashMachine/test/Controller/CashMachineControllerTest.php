<?php
namespace CashMachineTest\Controller;

use CashMachine\controller\CashMachineController;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use CashMachine\Model\CashMachine;
use Prophecy\Argument;

class CashMachineControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            // Grabbing the full application configuration:
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));
        parent::setUp();
    }
    
    public function testIndexActionMethodGetWithoutParams()
    {
        //Este debe de enviar un error debido a que en la api restful no está implementada la función de método get sin parametros (getList)
        echo "Cash-machine Method Get Without Params\n";
        $this->dispatch('/cash-machine');
        $this->assertResponseStatusCode(405);
        $this->assertModuleName('CashMachine');
        $this->assertControllerName(CashMachineController::class);
        $this->assertControllerClass('CashMachineController');
        $this->assertMatchedRouteName('CashMachine');
    }
    
    public function testAddActionWithdrawDebitAccountWithoutMoney()
    {
        echo "Withdraw Debit Account Without Money\n";
        $postData = [
            'id'     => '13',
            'amount' => '1000',
        ];
        $this->dispatch('/cash-machine', 'POST', $postData);
        $this->assertResponseStatusCode(500);
    }
    
    public function testAddActionWithdrawDebitAccountWithEnoughtMoney()
    {
        echo "Withdraw Debit Account With Enought Money\n";
        $postData = [
            'id'     => '15',
            'amount' => '1000',
        ];
        $this->dispatch('/cash-machine', 'POST', $postData);
        $this->assertResponseStatusCode(200);
    }
    
    public function testAddActionDepostDebitAccount()
    {
        echo "Depost Debit Account\n";
        $postData = [
            'amount' => '1000',
        ];
        $this->dispatch('/cash-machine?id=15', 'PUT', $postData);
        $this->assertResponseStatusCode(200);
    }
    
    public function testAddActionWithdrawCreditAccountWithoutMoney()
    {
        echo "Withdraw Credit Account Without Money, adding the commision\n";
        //Currently has $5000 available
        $postData = [
            'id'     => '16',
            'amount' => '5000',
        ];
        $this->dispatch('/cash-machine', 'POST', $postData);
        $this->assertResponseStatusCode(500);
    }
    
    public function testAddActionWithdrawCreditAccountWithEnoughtMoney()
    {
        echo "Withdraw Credit Account With Enought Money\n";
        $postData = [
            'id'     => '16',
            'amount' => '1000',
        ];
        $this->dispatch('/cash-machine', 'POST', $postData);
        $this->assertResponseStatusCode(200);
    }
    
    public function testAddActionDepostCreditAccountOverTheLimit()
    {
        echo "Depost Credit Account with more money over the limit\n";
        $postData = [
            'amount' => '11000', //The limit is 10000
        ];
        $this->dispatch('/cash-machine?id=16', 'PUT', $postData);
        $this->assertResponseStatusCode(500);
    }
    
    public function testAddActionDepostCreditAccountUnderTheLimit()
    {
        echo "Depost Credit Account with more money under the limit\n";
        $postData = [
            'amount' => '1100', //The limit is 10000
        ];
        $this->dispatch('/cash-machine?id=16', 'PUT', $postData);
        $this->assertResponseStatusCode(200);
    }
    
    public function testAddActionDepositDebitAccountAnNonExistent()
    {
        echo "Deposit Debit Account Non Existent\n";
        $postData = [
            'id'     => '-1',
            'amount' => '5000',
        ];
        $this->dispatch('/cash-machine', 'POST', $postData);
        $this->assertResponseStatusCode(500);
    }
    
}