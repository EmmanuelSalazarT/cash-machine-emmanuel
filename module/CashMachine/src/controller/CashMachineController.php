<?php
namespace CashMachine\controller;

use Zend\View\Model\JsonModel;

class CashMachineController  extends \Zend\Mvc\Controller\AbstractRestfulController 
{
    
    public function __construct(\Account\Model\DebitAccountTable $debitAccountTable, \Account\Model\CreditAccountTable $creditAccountTable)
    {
        \CashMachine\Model\CashMachineConector::setAccountTableConector($debitAccountTable);
        \CashMachine\Model\CashMachineConector::setCreditAccountTableConector($creditAccountTable);
    }
    
    /*
     * Get user's accounts by user's id
     * 
     * @params int $id, the id of user
     */
    public function get($id) 
    {
        $response = new \Application\Model\ResponseBuilder();
        try
        {
            $result = \CashMachine\Model\CashMachineConector::getAccountTableConector()->findByUserId($id);
            $response->createSuccesResponse();
            $response->setResponse($result);
            
        } catch (Exception $ex) {
            $response->createErrorResponse($ex->getMessage());
        }
        
        return new JsonModel( $response->buildJsonResponse());
    }
    
    
    /*Withdraw money
     * POST
     * 
     * @param JsonObject $data{
     *  "id":11, //id account
     *  "amount": 10000
     * }
     */
    public function create($data)
    {
        $response = new \Application\Model\ResponseBuilder();
        try
        {
            $result = \CashMachine\Model\CashMachine::withDraw($data["id"], $data["amount"]);
            $response->createSuccesResponse();
            $response->setResponse($result);
        } catch (Exception $ex) {
            $response->createErrorResponse($ex->getMessage());
        }
        
        return new JsonModel( $response->buildJsonResponse());
    }
    
    //deposit money
    /*PUT
     * 
     * @param int $id (By get); id account to deposit.
     * 
     * @param JsonObject $data (By post) {
     *      "amount": 10000
     * }
     */
    public function update( $id,$data)
    {
        $response = new \Application\Model\ResponseBuilder();
        try
        {
            $result = \CashMachine\Model\CashMachine::deposit($id, $data["amount"]);
            $response->createSuccesResponse();
            $response->setResponse($result);
        } catch (Exception $ex) {
            $response->createErrorResponse($ex->getMessage());
        }
        
        return new JsonModel( $response->buildJsonResponse());
    }
}