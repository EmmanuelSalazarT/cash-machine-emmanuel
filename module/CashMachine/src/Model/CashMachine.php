<?php
namespace CashMachine\Model;

class CashMachine
{
    public static function findAccount(int $idAcount)
    {
        $acount = \CashMachine\Model\CashMachineConector::getAccountTableConector()->getAccount($idAcount);
        if($acount->getType() == \Account\Model\AccountConstants::typeAccountCredit)
        {
            $acount = \CashMachine\Model\CashMachineConector::getCreditAccountTableConector()->getAccount($idAcount);
        }
        return $acount;
    }
    
    public static function deposit( int $idAcount, float $amount )
    {
        $acount = self::findAccount($idAcount);
        $result = $acount->deposit($amount);
        $acount->save();
        
        return $amount;
    }
    
    public static function withDraw(int $idAcount, float $amount)
    {
        $acount = self::findAccount($idAcount);
        $resul = $acount->withDraw($amount);
        $acount->save();
        
        return $amount;
    }
}