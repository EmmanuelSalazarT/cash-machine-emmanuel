<?php
namespace CashMachine\Model;

class CashMachineConector
{
    private static $accountTable;
    private static $creditAccountTable;
    
    public static function setAccountTableConector(\Account\Model\DebitAccountTable $debitAccountTable)
    {
        if(!isset(self::$accountTable))
        {
            self::$accountTable = $debitAccountTable;
        }
    }
    
    public static function getAccountTableConector()
    {
        return self::$accountTable;
    }
    
    public static function setCreditAccountTableConector(\Account\Model\CreditAccountTable $creditAccountTable)
    {
        if(!isset(self::$creditAccountTable))
        {
            self::$creditAccountTable = $creditAccountTable;
        }
    }
    
    public static function getCreditAccountTableConector()
    {
        return self::$creditAccountTable;
    }
}