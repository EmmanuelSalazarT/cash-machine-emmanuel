<?php
namespace CashMachine;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    
    public function getServiceConfig()
    {
        return [
            'factories' => [
                Account\Model\DebitAccountTable::class => function($container) {
                    $tableGateway = $container->get(Account\Model\DebitAccountTableGateway::class);
                    return new \Account\Model\DebitAccountTable($tableGateway);
                },
                Account\Model\DebitAccountTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Account\Model\DebitAccount());
                    return new TableGateway('account', $dbAdapter, null, $resultSetPrototype);
                },
                Account\Model\CreditAccountTable::class => function($container) {
                    $tableGateway = $container->get(Account\Model\CreditAccountTableGateway::class);
                    return new \Account\Model\CreditAccountTable($tableGateway);
                },
                Account\Model\CreditAccountTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Account\Model\CreditAccount());
                    return new TableGateway('credit_account', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
    
    public function getControllerConfig()
    {
        return [
            'factories' => [
            controller\CashMachineController::class => function($container) {
                    return new Controller\CashMachineController(
                        $container->get(Account\Model\DebitAccountTable::class),
                        $container->get(Account\Model\CreditAccountTable::class)
                    );
                },
            ],
        ];
    }
    
}