<?php
namespace Account\Controller;

use Zend\View\Model\JsonModel;

class AccountController extends \Zend\Mvc\Controller\AbstractRestfulController 
{   
    public function __construct(\Account\Model\DebitAccountTable $debitAccountTable, \Account\Model\CreditAccountTable $creditAccountTable)
    {
        \CashMachine\Model\CashMachineConector::setAccountTableConector($debitAccountTable);
        \CashMachine\Model\CashMachineConector::setCreditAccountTableConector($creditAccountTable);
    }
    
    public function get($id)
    {
        $response = new \Application\Model\ResponseBuilder();
        try
        {
            $account = \CashMachine\Model\CashMachine::findAccount($id);
            $response->createSuccesResponse();
            $response->setResponse($account);
        } catch (Exception $ex) {
            $response->createErrorResponse($ex->getMessage());
        }
        return new JsonModel( $response->buildJsonResponse());
    }
    
    public function getList()
    {
        $result = \CashMachine\Model\CashMachineConector::getAccountTableConector()->fetchAll();
        return new JsonModel( $result);
    }
    
    /*Create a new account
     * POST
     * 
     * @param JsonObject $data{
     *  "number": 13254325,
     *  "user_id": 1,
     *  "type": "DEBIT"
     * }
     * 
     * @param JsonObject $data{
     *  "number": 13254325,
     *  "user_id": 1,
     *  "type": "CREDIT",
     *  "amount_limit": 25000
     * }
     */
    public function create($data)
    {
        $response = new \Application\Model\ResponseBuilder();
        try
        {
            $number = (isset($data["number"]))? $data["number"] : null;
            $userId = (isset($data["user_id"]))? $data["user_id"] : null;
            $type = (isset($data["type"]))? $data["type"] : null;
            $amountLimit = (isset($data["amount_limit"]))? $data["amount_limit"] : null;
            
            $accountBuilder = new \Account\Model\AccountBuilder($userId, $number, $type);
            if($type == \Account\Model\AccountConstants::typeAccountCredit)
            {
                $accountBuilder->setAmountLimit($amountLimit);
            }
            $result = $accountBuilder->createAccount();
            $response->createSuccesResponse();
            $response->setResponse($result);
        } 
        catch (Exception $ex) 
        {
            $response->createErrorResponse($ex->getMessage());
        }
        
        return new JsonModel( $response->buildJsonResponse());
    }
    
}