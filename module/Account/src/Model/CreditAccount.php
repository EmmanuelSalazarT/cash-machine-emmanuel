<?php
namespace Account\Model;

class CreditAccount extends Account implements \JsonSerializable{
    
    protected $creditAccountId;
    protected $amountLimit;
    protected $commissionWithdraw = AccountConstants::defaultComission;
    
    //@override
    //private $type = AccountConstants::typeAccountCredit;
    
    public function exchangeArray($data)
    {
        parent::exchangeArray($data);
        $this->amountLimit     = (isset($data['amount_limit'])) ? $data['amount_limit'] : null;
        $this->commission_withdraw     = (isset($data['commission_withdraw'])) ? $data['commission_withdraw'] : null;
        $this->creditAccountId     = (isset($data['credit_account_id'])) ? $data['credit_account_id'] : null;
    }
    
    /*
     * Function to get the amount limit from the account
     * 
     * return float.
     */
    public function getAmountLimit()
    {
        return $this->amountLimit;
    }
    
    public function getCreditAccountId()
    {
        return $this->creditAccountId;
    }

    /*
     * Function to get the commission for withdraw.
     * 
     * return float; an amount between 0 and 1.
     */
    public function getCommissionWithdraw()
    {
        return $this->commissionWithdraw;
    }
    
    /*
     * Function to set the commision for withdraw.
     * 
     * @param Float $newCommissionWithdraw; must be between 0 and 1.
     */
    public function setCommissionWithdraw(Float $newCommissionWithdraw)
    {
        if($newCommissionWithdraw>1 || $newCommissionWithdraw<=0)
        {
            throw new \Exception('The commission must be between 0 and 1.');
        }
        $this->commissionWithdraw = $newCommissionWithdraw;
    }
    
    //@override
    /*
     * Override of function deposit. Add the condition for check if the amount availabne and the amount to deposit not suràss the amount limit of account
     * 
     * @param float $amount; the amount to deposit.
     */
    public function deposit(Float $amount)
    {
        if($amount + $this->amountAvailable<=$this->amountLimit)
        {
            parent::deposit($amount);
        }
        else
        {
            throw new \Exception('It\'s not possible deposit an amount higher than the amount to be paid.');
        }
    }
    
    //@override
    /*
     * Override of function withDraw. Add the comission to withDraw; just return the original $amount
     * 
     * @Param float $amount; original amount to withdraw without the commission.
     * 
     * return float; the original amount withdrawn, for give to user.
     */
    public function withDraw(Float $amount) 
    {
        $totalAmount = $amount + ($this->commissionWithdraw*$amount);
        parent::withDraw($totalAmount);
        return $amount;
    }
    
    /*
     * Function to change the current limit of amount.
     * 
     * param float @newAmount; the new amount for the account, must be higher than 0 and the amount to paid by de user.
     */
    public function changeAmountLimit(Float $newAmount)
    {
        if($newAmount>0)
        {
            if( !isset($this->amountLimit) || ($newAmount>= ($this->amountLimit - $this->amountAvailable) ) )
            {
                $this->amountLimit = $newAmount;
                $this->amountAvailable +=($newAmount - $this->amountAvailable);
            }
            else 
            {
                throw new \Exception('The new limit cannot be less than the current amount to be paid.');
            }
        }
        else
        {
            throw new \Exception('The new amount must be higher that 0 (zero).');
        }
    }
    
    protected function validateAttributesForSave()
    {
        if(!isset($this->amountLimit))
        {
            throw new \Exception('The limit of account it\'s not defined.');
        }
        return parent::validateAttributesForSave();
    }
    
    /*
     * Function to save the new account/changes in the database
     */
    public function save()
    {
        $this->validateAttributesForSave();
        parent::save();
        $result = \CashMachine\Model\CashMachineConector::getCreditAccountTableConector()->saveAccount($this);
        
        //...
    }
}