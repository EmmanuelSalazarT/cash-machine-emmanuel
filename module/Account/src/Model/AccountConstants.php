<?php
namespace Account\Model;

abstract class AccountConstants
{
    const typeAccountDebit = 'DEBIT';
    const typeAccountCredit = 'CREDIT';
    const typeAccountDefault = self::typeAccountDebit;
    
    const typesAccount = array(
        self::typeAccountCredit,
        self::typeAccountDebit,
    );
    
    const defaultComission = 0.1;
}