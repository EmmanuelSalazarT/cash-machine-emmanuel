<?php
namespace Account\Model;

class AccountBuilder
{
    private $account;
    
    public function __construct(int $userId,int $number, string $type)
    {
        switch ($type)
        {
            case AccountConstants::typeAccountDebit:
            {
                $this->account = new DebitAccount();
                break;
            }
            case AccountConstants::typeAccountCredit:
            {
                $this->account = new CreditAccount();
                break;
            }
        }
        $this->account->setUser($userId);
        $this->account->setNumber($number);
        $this->account->setType($type);
    }
    
    function setAmountLimit(float $amountLimit)
    {
        $this->account->changeAmountLimit($amountLimit);
        
        return $this;
    }
    
    function createAccount()
    {
        $this->account->save();
        
        return $this->account;
    }
}