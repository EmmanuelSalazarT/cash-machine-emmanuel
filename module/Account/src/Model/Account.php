<?php
namespace Account\Model;

class Account implements \JsonSerializable
{
    protected $id;
    protected $number;
    protected $userId;
    protected $amountAvailable = 0;
    protected $type;
    protected $status;
    
    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->number = (isset($data['number'])) ? $data['number'] : null;
        $this->userId = (isset($data['user_id'])) ? $data['user_id'] : null;
        $this->amountAvailable  = (isset($data['amount_available'])) ? $data['amount_available'] : null;
        $this->type  = (isset($data['type'])) ? $data['type'] : null;
        $this->status  = (isset($data['status'])) ? $data['status'] : null;
    }
    
    public function setId(int $id)
    {
        if($this->id == null || !$this->id)
        {
            $this->id = $id;
        }
    }
    
    /*
     * Function to Get the account's id.
     * 
     * return int; the account id.
     */
    public function getId()
    {
        return $this->id;
    }
    
    /*
     * Function to set for first time the number of account
     * 
     * @param int $number; the number of account; must have a length of 8 chars.
     */
    public function setNumber(Int $number)
    {
        if(isset($this->number))
        {
            throw new \Exception('The account currenly has a number.');
        }
        if(strlen((String)$number)!=8)
        {
            throw new \Exception('The account number must have a length of 8 chars.');
        }
        $this->number = $number;
    }
    
    /*
     * Function to set for first time the account's user.
     * 
     * @param User; the user's object to assign.
     */
    public function setUser(int $userId)
    {
        if(!isset($this->userId))
        {
            $this->userId = $userId;
        }
        else
        {
            throw new \Exception('The account currently has an user assigned.');
        }
    }
    
    public function getUserId()
    {
        return $this->userId;
    }


    /*
     * Function to get the account's number.
     * 
     * return Int.
     */
    public function getNumber()
    {
        return $this->number;
    }


    /*
     * Function to get the available ammount on de account;
     * 
     * return Float;
     */
    public function getAmountAvailable()
    {
        return $this->amountAvailable;
    }
    
    /*
     * Function to get the Type
     * 
     * return string (CREDIT,DEBIT...), The type of account from the types in "AccountConstants".;
     */
    public function getType()
    {
        return $this->type;
    }
    
    public function setType(string $type)
    {
        if(isset($this->type))
        {
            throw new \Exception('The type is already declared.');
        }
        if(!in_array($type, \Account\Model\AccountConstants::typesAccount))
        {
            throw new \Exception("The type $type it's not exists.");
        }
        $this->type = $type;
    }
    
    /*
     * Function to get the account's status
     * 
     * return boolean; the status of account (true=> active, false => innactive).
     */
    public function getStatus()
    {
        return $this->status?true:false;
    }
    
    public function setStatus(Bool $status)
    {
        $this->status = $status?1:0;
    }
    
    /*
     * Function to deposit an amount into de account
     * 
     * @param Float $amount; the amount to deposit, Must be higher than 0.
     */
    public function deposit(Float $amount)
    {
        if(!isset($this->id))
        {
            throw new \Exception('The account does not exist yet.');
        }
        if($amount>0)
        {
            $this->amountAvailable += $amount;
        }
        else
        {
            throw new \Exception('The amount must be higher that 0 (zero).');
        }
    }
    
    /*
     * Function to withdraw an amount from the account
     * 
     * @param Float $amount the amount to withdraw; must be higher than 0; the account must have the account available.
     * 
     * return Float, the amount withdrawn to the user.
     */
    public function withDraw(Float $amount)
    {
        if($amount>0)
        {
            if($this->amountAvailable>=$amount)
            {
                $this->amountAvailable -= $amount;
                return $amount;
            }
            else
            {
                throw new \Exception('The requested amount is not available.');
            }
        }
        else
        {
            throw new \Exception('The amount to request must be higher than 0 (zero).');
        }
    }
    
    /*
     * Function to validate the account's attributes before to save
     * 
     * return Boolean in case all it's ok; exception if missing one o more attributes for set.
     */
    protected function validateAttributesForSave()
    {
        if(!isset($this->number))
        {
            throw new \Exception('Missing the account number.');
        }
        if(!isset($this->userId))
        {
            throw new \Exception('Missing the account own (User).');
        }
        if(!isset($this->type))
        {
            throw new \Exception('The account type it\'s not declared.');
        }
        return true;
    }
    
    /*
     * Function to save the new account/changes in the database
     */
    public function save()
    {
        $this->validateAttributesForSave();
        
        $result = \CashMachine\Model\CashMachineConector::getAccountTableConector()->saveAccount($this);
    }
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}