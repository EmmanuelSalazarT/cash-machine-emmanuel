<?php
namespace Account\Model;

use Zend\Db\TableGateway\TableGatewayInterface;

class CreditAccountTable
{
    protected $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet =  $this->tableGateway->select();
        $resultArray = array();
        foreach($resultSet as $row)
        {
            $resultArray[] = $row;
        }
        return $resultArray;
    }

    public function getAccount($id)
    {
        $id  = (int) $id;
        $sql = new \Zend\Db\Sql\Sql( $this->tableGateway->adapter ) ;
        
        $where = new \Zend\Db\Sql\Where();
        $where -> equalTo( 'account_id', $id ) ;
        $select = $sql->select() ;
        $select -> from ( $this->tableGateway->getTable() )
            ->columns(array("id as credit_account_id","account_id","amount_limit","commission_withdraw"))
            -> join ( 'account' , 'account.id = credit_account.account_id',array("id","amount_available","user_id","type","status","number"),'left')
            -> where( $where ) ;
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        $creditAccount = new \Account\Model\CreditAccount();
        $creditAccount->exchangeArray($row);
        return $creditAccount;
    }

    public function saveAccount(CreditAccount $account)
    {
        $data = array(
            'amount_limit' => ($account->getAmountLimit()!= null )?$account->getAmountLimit():null,
            'commission_withdraw' => ($account->getCommissionWithdraw()!=null)?$account->getCommissionWithdraw():null,
            'account_id'  => $account->getId(),
            'id' => $account->getCreditAccountId(),
        );
        if ($account->getCreditAccountId()!=null && $account->getCreditAccountId()>0 ) {
             $this->tableGateway->update($data, array('id' => $account->getCreditAccountId()));
        } else {
             $this->tableGateway->insert($data);
        }
    }

    public function deleteAccount(Account $account)
    {
        $account->setStatus(false);
        $this->saveAccount($account);
    }
    
}