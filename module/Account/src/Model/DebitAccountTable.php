<?php
namespace Account\Model;

use Zend\Db\TableGateway\TableGatewayInterface;

class DebitAccountTable
{
    protected $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet =  $this->tableGateway->select();
        $resultArray = array();
        foreach($resultSet as $row)
        {
            $resultArray[] = $row;
        }
        return $resultArray;
    }

    public function getAccount($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function findByUserId($userId)
    {
        $resultSet =  $this->tableGateway->select(['user_id' => $userId]);
        $resultArray = array();
        foreach($resultSet as $row)
        {
            $resultArray[] = $row;
        }
        return $resultArray;
    }

    public function saveAccount(Account $account)
    {
        $data = array(
            'id' => ($account->getId()!= null )?$account->getId():null,
            'number' => $account->getNumber(),
            'amount_available'  => $account->getAmountAvailable(),
            'user_id' => $account->getUserId(),
            'type' => $account->getType(),
        );
        if ($account->getId()!=null && $account->getId()>0 ) {
             $this->tableGateway->update($data, array('id' => $account->getId()));
             return $account;
        } else {
             $this->tableGateway->insert($data);
             $account->setId($this->tableGateway->lastInsertValue);
        }
    }

    public function deleteAlbum(Account $account)
    {
        $account->setStatus(false);
        $this->saveAccount($account);
    }
}