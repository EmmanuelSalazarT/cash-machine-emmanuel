<?php
namespace Account;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    
    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\DebitAccountTable::class => function($container) {
                    $tableGateway = $container->get(Model\DebitAccountTableGateway::class);
                    return new Model\DebitAccountTable($tableGateway);
                },
                Model\DebitAccountTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\DebitAccount());
                    return new TableGateway('Account', $dbAdapter, null, $resultSetPrototype);
                },
                Model\CreditAccountTable::class => function($container) {
                    $tableGateway = $container->get(Model\CreditAccountTableGateway::class);
                    return new Model\CreditAccountTable($tableGateway);
                },
                Model\CreditAccountTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\CreditAccount());
                    return new TableGateway('credit_account', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
    
    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\AccountController::class => function($container) {
                    return new Controller\AccountController(
                        $container->get(Model\DebitAccountTable::class),
                        $container->get(Model\CreditAccountTable::class)
                    );
                },
            ],
        ];
    }
}