<?php
namespace Application\Model;

class ResponseBuilder
{
    protected $response;
    
    public function __construct() {
        
    }
    
    public function createErrorResponse(string $message)
    {
        $this->response = new Response();
        $this->response->setMessage($message);
        $this->response->setStatus(ApplicationConstants::responseError);
    }

    public function createSuccesResponse(string $message = null)
    {
        $this->response = new Response();
        $this->response->setMessage($message);
        $this->response->setStatus(ApplicationConstants::responseSucces);
    }
    
    public function setResponse($response)
    {
        $this->response->setResponse($response);
    }
    
    public function buildJsonResponse()
    {
        return array(
            "status" => $this->response->getStatus(),
            "message" => $this->response->getMessage(),
            "response" => $this->response->getResponse(),
        );
    }
}