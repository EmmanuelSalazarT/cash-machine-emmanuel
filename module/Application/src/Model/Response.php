<?php
namespace Application\Model;

class Response
{
    protected $status;
    protected $message;
    protected $response;
    
    function getStatus() {
        return $this->status;
    }

    function getCode() {
        return $this->code;
    }

    function getMessage() {
        return $this->message;
    }

    function getResponse() {
        return $this->response;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setCode($code) {
        $this->code = $code;
    }

    function setMessage($message) {
        $this->message = $message;
    }

    function setResponse($response) {
        $this->response = $response;
    }

}