<?php
namespace Application\Model;

abstract class ApplicationConstants
{
    const responseSucces = 'SUCCESS';
    const responseError = 'ERROR';
}