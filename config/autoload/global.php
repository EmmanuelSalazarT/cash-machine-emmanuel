<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'db' => [
        'driver' => 'Pdo',
        //'dsn'    => sprintf('sqlite:%s/data/zftutorial.db', realpath(getcwd())),
        'dsn'    => 'mysql:dbname=cash_machine;host=localhost;',
        
        //Agrego estos valores en el globa.php debido a que será para presentación; se recomienda que, en un ambiente real, esto se agrege al archivo local.php el cual debe ser ignorado por git.
        'username' => 'cash_machine_sys',
        'password' => 'Habrá que cambiarla consecutivamente.',
    ],
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
                    => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
];